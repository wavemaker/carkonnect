Application.$controller("TripsPageController", ["$scope", "$rootScope", function($scope, $rootScope) {
    "use strict";

    $scope.onPageReady = function() {
        $scope.Widgets.spinner2.show = true;

        // checking for login status of user
        if ($rootScope.loginStatus === false)
            $rootScope.Variables.Logout.navigate();

    };


    $scope.MojInvokeonSuccess = function(variable, data) {

        var arr = data.Data;
        var min = 0,
            max = 0,
            average = 0;
        var go = arr.length - 1;

        /*-------------------------------------------WAY POINTS--------------------------------------------------*/
        average = [];
        for (var i = 0; i < go && i < 58; i++) {
            if (i % 10 == 0) {
                average.push({
                    location: {
                        lat: arr[i].Location.Lat,
                        lng: arr[i].Location.Lng
                    }
                });
            }
        }
        $scope.Variables.WayPoints.dataSet = average;

        /*-------------------------------------------SPEED----------------------------------------------------*/
        min = arr[0].Speed;
        max = arr[0].Speed;
        average = 0;

        for (var i = 0; i < go; i++) {
            if (arr[i].Speed < min) min = arr[i].Speed;
            if (arr[i].Speed > max) max = arr[i].Speed;
            average += arr[i].Speed;
        }
        average /= arr.length;
        $scope.minSpeed = min;
        $scope.maxSpeed = max;
        $scope.averageSpeed = average;
        /*-------------------------------------------RPM---------------------------------------------------------*/
        min = arr[0].RPM;
        max = arr[0].RPM;
        average = 0;
        for (var i = 0; i < go; i++) {
            if (arr[i].RPM < min) min = arr[i].RPM;
            if (arr[i].RPM > max) max = arr[i].RPM;
            average += arr[i].RPM;
        }
        average /= arr.length;

        $scope.minRPM = min;
        $scope.maxRPM = max;
        $scope.averageRPM = average;
        /*-------------------------------------------FUEL LEVEL--------------------------------------------------*/
        min = arr[0].FuelLevel;
        max = arr[0].FuelLevel;
        average = 0;
        for (var i = 0; i < go; i++) {
            if (arr[i].FuelLevel < min) min = arr[i].FuelLevel;
            if (arr[i].FuelLevel > max) max = arr[i].FuelLevel;
        }
        $scope.startingFuelLevel = max;
        $scope.endingFuelLevel = min;
        /*-------------------------------------------TIME---------------------------------------------------*/
        min = arr[0].Time;
        max = arr[0].Time;
        average = 0;
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].Time < min) min = arr[i].Time;
            if (arr[i].Time > max) max = arr[i].Time;
        }
        $scope.startingTime = min;
        $scope.endingTime = max;
        var timeTaken = new Date(max).getTime() - new Date(min).getTime();
        //Formating
        var d, h, m, s;
        s = Math.floor(timeTaken / 1000);
        m = Math.floor(s / 60);
        s = s % 60;
        h = Math.floor(m / 60);
        m = m % 60;
        d = Math.floor(h / 24);
        h = h % 24;

        $scope.time = d + "d," + h + "hrs," + m + "min," + s + "sec";
        if (d == 0)
            $scope.time = h + "hrs, " + m + "min, " + s + "sec";
        if (h == 0)
            $scope.time = m + "min, " + s + "sec";
        if (m == 0)
            $scope.time = s + "sec";

        $scope.Widgets.spinner2.show = false;
    };


    $scope.MojSelectedTripIdInvokeonSuccess = function(variable, data) {
        $scope.parseDate = moment(data.StartTime).format('Do');
    };

}]);