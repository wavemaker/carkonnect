Application.$controller("TripsNamesPageController", ["$scope", "$rootScope", function($scope, $rootScope) {
    "use strict";
    $scope.onPageReady = function() {

        // checking for login status of user
        if ($rootScope.loginStatus === false) {
            $rootScope.Variables.Logout.navigate();
        }

        // checking for re-login 
        if (!$rootScope.userFetched) {
            $scope.Variables.MojCurrentUserInvoke.update();
        }

        // checking for user trips count
        if ($rootScope.tripsCount == 0)
            $scope.Widgets.containerNoTrips.show = true;
        else
            $scope.Widgets.containerNoTrips.show = false;
    };


    $scope.TipslistTap = function($event, $isolateScope) {
        $scope.Variables.SelectedTrip.dataSet.dataValue = $scope.Widgets.Tipslist.selecteditem._id;
    };

    $scope.MojCurrentUserInvokeonSuccess = function(variable, data) {
        $rootScope.userFetched = true;
        $scope.Widgets.spinner1.show = true;
        $scope.Variables.MojUserTripsInvoke.setInput({
            "UserId": data._id,
            "MojioAPIToken": $rootScope.tokenHeader
        });
        $scope.Variables.MojUserVehiclesInvoke.setInput({
            "UserId": data._id,
            "MojioAPIToken": $rootScope.tokenHeader
        });
        $scope.Variables.MojUserTripsInvoke.update();
        $scope.Variables.MojUserVehiclesInvoke.update();
        if (data.FirstName == undefined || data.LastName == undefined)
            $rootScope.User = data.UserName;
        else {
            $rootScope.User = data.FirstName + " " + data.LastName;
        }

    };

    $scope.MojUserTripsInvokeonSuccess = function(variable, data) {
        $rootScope.tripsList = data.Data;
        $rootScope.tripsCount = data.TotalRows;
        if ($rootScope.tripsCount == 0)
            $scope.Widgets.containerNoTrips.show = true;
        $scope.Widgets.spinner1.show = false;
    };


    $scope.MojUserVehiclesInvokeonSuccess = function(variable, data) {
        $scope.Variables.UserVehicles.dataSet = data.Data;
        $rootScope.vehiclesCount = data.TotalRows;
    };

}]);