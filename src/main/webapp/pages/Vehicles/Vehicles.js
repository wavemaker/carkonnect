Application.$controller("VehiclesPageController", ["$scope", "$rootScope", function($scope, $rootScope) {
    "use strict";

    $scope.onPageReady = function() {
        // checking for login status of user
        if ($rootScope.loginStatus === false)
            $rootScope.Variables.Logout.navigate();
    };


    $scope.MojVehiclesById1InvokeonSuccess = function(variable, data) {
        if ((data.LastTrip == undefined) && ($rootScope.vehiclesCount != 0)) {
            $scope.Widgets.container1.show = true;
            $scope.Widgets.panel2.show = false;
            $scope.Widgets.panel3.show = false;
        } else {
            $scope.Widgets.container1.show = false;
            $scope.Widgets.panel2.show = true;
            $scope.Widgets.panel3.show = true;
        }
    };

}]);