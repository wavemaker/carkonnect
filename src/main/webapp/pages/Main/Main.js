Application.$controller("MainPageController", ["$scope", "$rootScope", "wmMojio", "wmDemoMojio", "wmToaster", function($scope, $rootScope, wmMojio, wmDemoMojio, wmToaster) {
    "use strict";



    $scope.onPageReady = function() {
        // Checking for First Run of the App
        var run = localStorage.getItem('FirstRun');
        if (run === null) {
            localStorage.setItem('FirstRun', 'true');
            $scope.Widgets.anchorSkip.show = true;
            $scope.Widgets.carousel2.show = true;

            //swip for carousel
            var carousel = document.querySelector('[uib-carousel]');
            var carouselScope = $('[data-identifier="carousel"] [no-wrap="noWrapSlides"]').isolateScope();

            Hammer(carousel).on("swipeleft", function() {
                carouselScope.next();
                carouselScope.$apply();
            });

            Hammer(carousel).on("swiperight", function() {
                carouselScope.prev();
                carouselScope.$apply();
            });
        }
    };

    $scope.buttonLoginTap = function($event, $isolateScope) {
        if (navigator.onLine)
            wmMojio.login('54faac50-7efa-4de2-95fb-8daec3649917');
        else
            wmToaster.show('info', 'INFO', 'You are Offline', 5000);
    };
    $scope.anchorSkipTap = function($event, $isolateScope) {
        $scope.Widgets.carousel2.show = false;
        $scope.Widgets.anchorSkip.show = false;
    };



    $scope.buttonDemoLoginTap = function($event, $isolateScope) {
        if (navigator.onLine)
            wmDemoMojio.login('54faac50-7efa-4de2-95fb-8daec3649917');
        else
            wmToaster.show('info', 'INFO', 'You are Offline', 5000);
    };

}]);