Application.$controller("LoginPageController", ["$scope", "wmMojio",
    function($scope, wmMojio) {
        "use strict";
        /* perform any action on the variables within this block(on-page-load) */
        $scope.onPageVariablesReady = function() {
            /*
             * variables can be accessed through '$scope.Variables' property here
             * e.g. to get data in a static variable named 'loggedInUser' use following script
             * $scope.Variables.loggedInUser.getData()
             */
        };
        /* perform any action on widgets within this block */
        $scope.onPageReady = function() {
            /*
             * widgets can be accessed through '$scope.Widgets' property here
             * e.g. to get value of text widget named 'username' use following script
             * '$scope.Widgets.username.datavalue'
             */
        };

        $scope.button2Tap = function($event, $isolateScope) {
            wmMojio.login('54faac50-7efa-4de2-95fb-8daec3649917');
        };

    }
]);