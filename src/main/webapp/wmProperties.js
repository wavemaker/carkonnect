var _WM_APP_PROPERTIES = {
  "activeTheme" : "orange-juice1",
  "defaultLanguage" : "en",
  "displayName" : "CarKonnect",
  "homePage" : "Main",
  "name" : "CarKonnect",
  "platformType" : "MOBILE",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.0"
};