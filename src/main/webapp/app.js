 Application.run(function($rootScope, wmMojio, wmDemoMojio, wmToaster) {
     "use strict";

     $rootScope.onAppVariablesReady = function() {};
     $rootScope.DeviceInfoonSuccess = function(variable, data) {
         $rootScope.loginStatus = false;
         //  TTS.speak('Hi,    Welcome to CarConnect   Powered by WaveMaker', function() {});
     };
     $rootScope.toDay = Date.parse(new Date().toDateString());


     /**********************Checking for online, if not user is logged out********/
     setInterval(CheckOnline, 1000);
     $rootScope.isLoggedout = false;

     function CheckOnline() {
         if (!navigator.onLine) {
             if (!$rootScope.isLoggedout) {
                 $rootScope.loginStatus = false;
                 $rootScope.isLoggedout = true;
                 $rootScope.Variables.Logout.navigate();
                 wmToaster.show('info', 'INFO', 'You are Offline', 5000);
             }
         }
     }


 });
 document.addEventListener("backbutton", onBackKeyDown, false);

 function onBackKeyDown() {
     // Handle the back button

     if (window.location.hash == "#/TripsNames") {
         if (navigator) {
             navigator.Backbutton.goHome(function() {

                 console.log('success')
             }, function() {
                 console.log('fail')
             });
         }
     } else {
         window.history.go(-1);
     }

 }



 /**********************Factory Method for Obataining oAuth token form URL in In-App Browser********/
 Application.factory('wmMojio', function($q, $rootScope) {
     function loginMojio(clientID) {
         var deferred = $q.defer();
         if (window.cordova) {
             if (true) {
                 //call_back url             
                 var redirect_uri = "https://my.moj.io/#access_token=";
                 //InappBrowser method to excute login page in Application Browser
                 var browserRef = window.cordova.InAppBrowser.open('https://accounts.moj.io/account/signin/mojio?returnUrl=%2Foauth2%2Fauthorize%3FGuid%3D8c56b926-d00f-48c3-8db8-88bc8be2f75d%26client_id%3D9852c940-19bb-47d0-9a7b-b9ec89776d14%26redirect_uri%3Dhttps%3A%2F%2Fmy.moj.io%2F%26response_type%3Dtoken%26scope%3Dlegacy%2520full%26prompt%3Dlogin', '_blank', 'location=no,clearsessioncache=yes,clearcache=yes');

                 browserRef.addEventListener('loadstart', function(event) {
                     if ((event.url).indexOf(redirect_uri) === 0) {

                         var result_token = event.url.split("access_token=")[1].split("&")[0];
                         localforage.setItem('token', result_token).then(function() {
                             localforage.getItem('token').then(function(data) {
                                 $rootScope.tokenHeader = data;
                                 if (data != "") {
                                     $rootScope.loginStatus = true;
                                     $rootScope.isLoggedout = false;
                                     $rootScope.Variables.goToPage_TripsNames.navigate();
                                 }
                             });
                         });
                         //Exit the InAppBrowser  
                         browserRef.removeEventListener("exit", function(event) {});
                         browserRef.close();
                     }
                 });
             } else {
                 deferred.reject("Could not find InAppBrowser plugin");
             }
         } else {
             deferred.reject("Cannot authenticate via a web browser");
         }
         return deferred.promise;
     }
     return {
         login: loginMojio
     };
 });

 /**********************Factory Method(Demo Login) for Obataining oAuth token form URL in In-App Browser********/

 Application.factory('wmDemoMojio', function($q, $rootScope) {
     function loginMojio(clientID) {
         var deferred = $q.defer();
         if (window.cordova) {
             if (true) {
                 //call_back url             
                 var redirect_uri = "https://my.moj.io/#access_token=";
                 //InappBrowser method to excute login page in Application Browser
                 var browserRef = window.cordova.InAppBrowser.open('https://accounts.moj.io/account/signin/mojio?returnUrl=%2Foauth2%2Fauthorize%3FGuid%3D8c56b926-d00f-48c3-8db8-88bc8be2f75d%26client_id%3D9852c940-19bb-47d0-9a7b-b9ec89776d14%26redirect_uri%3Dhttps%3A%2F%2Fmy.moj.io%2F%26response_type%3Dtoken%26scope%3Dlegacy%2520full%26prompt%3Dlogin', '_blank', 'location=no,clearsessioncache=yes,clearcache=yes');
                 browserRef.addEventListener('loadstop', function(event) {
                     browserRef.executeScript({
                         code: " $('.group label').remove();document.getElementsByTagName('input')[2].value='demoapps@wavemaker.com';document.getElementsByTagName('input')[3].value='pramati';$('.topcard [type=submit]').click();"

                     });
                 });
                 browserRef.addEventListener('loadstart', function(event) {

                     if ((event.url).indexOf(redirect_uri) === 0) {

                         var result_token = event.url.split("access_token=")[1].split("&")[0];
                         localforage.setItem('token', result_token).then(function() {
                             localforage.getItem('token').then(function(data) {
                                 $rootScope.tokenHeader = data;
                                 if (data != "") {
                                     $rootScope.loginStatus = true;
                                     $rootScope.isLoggedout = false;
                                     $rootScope.Variables.goToPage_TripsNames.navigate();
                                 }
                             });
                         });
                         //Exit the InAppBrowser  
                         browserRef.removeEventListener("exit", function(event) {});
                         browserRef.close();
                     }
                 });
             } else {
                 deferred.reject("Could not find InAppBrowser plugin");
             }
         } else {
             deferred.reject("Cannot authenticate via a web browser");
         }
         return deferred.promise;
     }
     return {
         login: loginMojio
     };
 });